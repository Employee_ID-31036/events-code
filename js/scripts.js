(async function () {
    let data = [];
    data = await fetchDataJSON();
    //let Alltabcontent = document.getElementById("All");
    //let SocialTabcontent = document.getElementById("Social");
   // let CloudTabcontent = document.getElementById("Cloud");
    //let DataTabcontent = document.getElementById("Data");
   // let DevopsTabcontent = document.getElementById("DevOps");
    loadContentIntTab(data, "All");
    loadContentIntTab(data, "Social");
    loadContentIntTab(data, "Cloud");
    loadContentIntTab(data, "Data");
    loadContentIntTab(data, "DevOps");
  })();
   
  function loadContentIntTab(data, tabName) {
    data.forEach(event => {
      tabcontent = document.getElementById(tabName);
      if (event.category.includes(tabName) || tabName === 'All') {
        const icon = document.createElement("i");
        icon.style.position = "absolute";
        icon.style.padding = "3px";
        icon.style.backgroundColor = event.liked ? "red" : "grey";
        icon.style.color = "white";
        icon.style.borderColor = "white";
        icon.style.top = "15px";
        icon.style.right = "15px";
        icon.style.borderRadius = "50px";
        icon.className = "fa fa-heart-o display-topright ";
        icon.id = "iconid";
        icon.addEventListener("click", function () {
          event.liked = !event.liked;
          if (event.liked) {
            icon.style.backgroundColor = "red";
          } else {
            event.liked = false;
            icon.style.backgroundColor = "grey";
          }
        });
        const mainContainerDiv = document.createElement("div");
        mainContainerDiv.className="col l3 m6 margin-bottom";
        const containerDiv = document.createElement("div");
        containerDiv.className="display-container";
        const image = document.createElement("img");
        let number = Math.floor(Math.random()*8);
        setCodeSrc="imgs/Rectangle"+[number]+".jpg"; 
        image.src = setCodeSrc;
        image.style.width="100%";
        image.alt ="event";
        const place = document.createElement("div");
        place.innerHTML = event.event_name;
        place.style.fontSize="15px";
        const time = document.createElement("div");
        time.innerHTML = event.date;
        time.style.color="red";
        time.style.fontSize="10px";
        mainContainerDiv.appendChild(containerDiv);
        containerDiv.appendChild(icon);
        containerDiv.appendChild(image);
        containerDiv.appendChild(place);
        containerDiv.appendChild(time);
        tabcontent.appendChild(mainContainerDiv);
      }
    });
  }
  async function fetchDataJSON() {
    const response = await fetch('./Events.json');
    const events = await response.json();
    return events;
  }
  async function openEvents(evt, tabName) {
    const target = evt.target;
    const parent = target.parentNode;
    const grandparent = parent.parentNode;
  
    // Remove all current selected tabs
    parent
      .querySelectorAll('[aria-selected="true"]')
      .forEach((t) => t.setAttribute("aria-selected", false));
  
    // Set this tab as selected
    target.setAttribute("aria-selected", true);
  
  
    data = await fetchDataJSON();
    var i, tabcontent, tablinks;
    tabcontent = document.getElementsByClassName("tabcontent");
    for (i = 0; i < tabcontent.length; i++) {
      tabcontent[i].style.display = "none";
    }
    tablinks = document.getElementsByClassName("tablinks");
    for (i = 0; i < tablinks.length; i++) {
      tablinks[i].className = tablinks[i].className.replace(" active", "");
      tablinks[i].id = tablinks[i].id.replace("defaultOpen", ""); 
    }
    document.getElementById(tabName).style.display = "block";
    evt.target.className += " active";
  } 
  window.addEventListener("DOMContentLoaded", () => {
    const tabs = document.querySelectorAll('[role="tab"]');
    const tabList = document.querySelector('[role="tablist"]');
  
    // Add a click event handler to each tab
    tabs.forEach((tab) => {
      tab.addEventListener("click", changeTabs);
    });
  
    // Enable arrow navigation between tabs in the tab list
    let tabFocus = 0;
  
    tabList.addEventListener("keydown", (e) => {
      // Move right
      if (e.key === "ArrowRight" || e.key === "ArrowLeft") {
        tabs[tabFocus].setAttribute("tabindex", -1);
        if (e.key === "ArrowRight") {
          tabFocus++;
          // If we're at the end, go to the start
          if (tabFocus >= tabs.length) {
            tabFocus = 0;
          }
          // Move left
        } else if (e.key === "ArrowLeft") {
          tabFocus--;
          // If we're at the start, move to the end
          if (tabFocus < 0) {
            tabFocus = tabs.length - 1;
          }
        }
  
        tabs[tabFocus].setAttribute("tabindex", 0);
        tabs[tabFocus].focus();
      }
    });
  });
  